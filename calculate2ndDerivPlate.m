function [d1,d2,x1,x2,maxd2,Cq] = ...
    calculate2ndDerivPlate(fname,nwells,windowSize,sheetNumber,thresh,outfname)
% [d1,d2,x1,x2,maxd2,Cq] = ...
%     calculate2ndDerivPlate(fname,nwells,windowSize,sheetNumber,thresh,outfname)
% 
% Calculate second derivatives of data in a BioRad CFX 96 or CFX 384
% amplification curve file ("...Quantification Amplification
% Results.xlsx"), and get Cq values by local quadratic fitting of the
% maximum second derivative
% 
% Outputs:
% d1 - 3-dimensional array of first derivatives for each well (A1, A2,
% % ..., B1, B2, etc.) Dimension 1 = row, dimension 2 = column, dimension 3 =
% % cycle number
% d2 - 3-dimensional array of second derivatives for each well (A1, A2,
% % ..., B1, B2, etc.) Dimension 1 = row, dimension 2 = column, dimension 3 =
% % cycle number
% x1 - cycle numbers for first derivatives
% x2 - cycle numbers for second derivatives
% maxd2 - maximum second derivative 
% Cq - 2-dimensional array of Cq values
% 
% Inputs:
% fname - xlsx input file name
% nwells - number of wells (either 96 or 384)
% windowSize - size of the window to use for calculating moving average
% sheetNumber - number of the sheet in the xlsx document in which the
% % data are stored. Set to 1 if you are using a single fluorescence 
% % channel.
% thresh (optional) - second difference threshold for scoring amplification.
% % To have no minimum threshold, you can either omit this argument or set
% % it to % [] or 0.
% outfname (optional) - file name for saving output (.mat file).
% 
% Thomas Graham, Tjian-Darzacq lab, 9-2-2020


% read in data from xls file
results = xlsread(fname,sheetNumber);

if nwells == 96
    nrows = 8;
    ncols = 12;
elseif nwells == 384
    nrows = 16;
    ncols = 24;    
else
    disp('Error in calculate2ndDerivPlate: nwells must be either 96 or 384.')
    return;    
end

% reshape data into 3D array
currIndex = 2;
for j=1:nrows
    for k=1:ncols
        data(j,k,:) = results(:,currIndex);
        currIndex = currIndex + 1;
    end
end


x1 = (windowSize+0.5):(size(data,3)-windowSize+0.5);
x2 = (2*windowSize):(size(data,3)-2*windowSize+1);
d1 = zeros(nrows,ncols,size(data,3)-2*windowSize+1);
d2 = zeros(nrows,ncols,size(data,3)-4*windowSize+2);
maxd2 = zeros(nrows,ncols);
Cq = zeros(nrows,ncols);

% calculate 1st and 2nd derivatives and Cq value for each fluorescence
% trace
for j=1:nrows
    for k=1:ncols
        currData = data(j,k,:);
        currData = currData(:);
        if exist('thresh','var') && ~isempty(thresh) && isnumeric(thresh)
            [~,currd1,~,currd2,currmaxd2,c] = calculate2ndDeriv(currData,windowSize,thresh);
        else
            [~,currd1,~,currd2,currmaxd2,c] = calculate2ndDeriv(currData,windowSize,[]);
        end
        d1(j,k,:) = currd1;
        d2(j,k,:) = currd2;
        maxd2(j,k) = currmaxd2;
        Cq(j,k) = c;
    end
end

% % optionally, apply a threshold to the Cq values and accept only those that
% % are below a certain value
% if exist('thresh','var') && ~isempty(thresh) && isnumeric(thresh)
%     selector = maxd2 >= thresh;
%     Cq(~selector) = nan;
% end

if exist('outfname','var')
    save(outfname,'x1','x2','d1','d2','maxd2','Cq')
end

end

























