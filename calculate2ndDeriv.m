function [x1, d1, x2, d2, maxd2, Cq] = calculate2ndDeriv(y,w,thresh)
% [x1, d1, x2, d2, maxd2, Cq] = calculate2ndDeriv(y,w,thresh)
% 
% Calculate first and second derivatives of y, averaging over a window of
% size w, and determine the peak of the second derivative (Cq) by fitting
% locally to a parabola. This is useful as an alternative to thresholding
% for determining quantification cycle (Cq) values in qPCR data.
% 
% Assumes that the x values of the function y are 1, 2, 3, ...
% e.g., qPCR cycle number.
% 
% Outputs:
% x1 - x values for first derivative. 
% d1 - first derivative
% x2 - x values for second derivative
% d2 - second derivative
% maxd2 - maximum value of second derivative
% Cq - Cq value (cycle at which second derivative is maximum, based on
% % parabolic fit to the d2 curve. If the maximum of d2 is at
% % the last cycle, then this is taken to be the Cq value.
% 
% Inputs:
% y - amplification trace (1-dimensional vector)
% w - sliding window size to use for calculating first and second
% % differences
% thresh (optional) - second-derivative threshold; if maxd2 is below this
% % value, then fitting will not be performed, and the Cq value will be
% % returned as NaN
% 
% Note: x2 and d2 are shorter than x1 and d1, which are shorter than the
% input vector y. This is because at least w points before and w points
% after are required to calculate the derivative.
% 
% Thomas Graham, Tjian-Darzacq lab, 9-2-2020


y = y(:);

% linear filter that gives the difference between the average of the w
% preceding points and the w subsequent points
kern = [ones(w,1); -ones(w,1)]/w;  

% convolve twice with differentiating filter
d1 = conv(y,kern,'valid')/w; % dY/dX ~ deltaY / deltaX
x1 = (w+0.5):(numel(y)-w+0.5);
x1 = x1'; 
d2 = conv(d1,kern,'valid')/w; % d2Y/dX2 ~ delta(dY/dX) / deltaX
x2 = (2*w):(numel(y)-2*w+1);
x2 = x2';

% determine maximum of second derivative
[maxd2,Cq] = max(d2);

% set minimum and maximum x value for fitting to a parabola
rangeMin = max(1,Cq-w);
rangeMax = min(numel(d2),Cq+w);

% return a Cq value only if the second derivative is above the specified
% threshold, thresh
% If thresh is not provided by the user, return a Cq value by default
if exist('thresh','var') && ~isempty(thresh) && maxd2 < thresh
    Cq = nan;
else
    % if the highest second derivative is at the very end, take that value as
    % the Cq
    if Cq == numel(d2) 
        Cq = x2(end);
    % otherwise, fit to a parabola to determine the Cq with sub-cycle
    % resolution
    else
        f = fit(x2(rangeMin:rangeMax),d2(rangeMin:rangeMax),'poly2');
        Cq = -f.p2/(2*f.p1);
    end
end

end