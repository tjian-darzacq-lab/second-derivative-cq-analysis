Code for determining Cq values from qPCR data using the second-derivative method.

calculate2ndDeriv takes in a 1-dimensional fluorescence trace and returns the numerical first and second derivatives and a calculated Cq value (peak of 2nd derivative)

calculate2ndDerivPlate applies calculate2ndDeriv to the "Quantification Amplification
Results.xlsx" output file from a BioRad CFX96 or CFX384 real-time PCR instrument.

Thomas Graham, Tjian-Darzacq lab, 9-2-2020

----------------------------------------------------------------------------------------

% [x1, d1, x2, d2, maxd2, Cq] = calculate2ndDeriv(y,w,thresh)
% 
% Calculate first and second derivatives of y, averaging over a window of
% size w, and determine the peak of the second derivative (Cq) by fitting
% locally to a parabola. This is useful as an alternative to thresholding
% for determining quantification cycle (Cq) values in qPCR data.
% 
% Assumes that the x values of the function y are 1, 2, 3, ...
% e.g., qPCR cycle number.
% 
% Outputs:
% x1 - x values for first derivative. 
% d1 - first derivative
% x2 - x values for second derivative
% d2 - second derivative
% maxd2 - maximum value of second derivative
% Cq - Cq value (cycle at which second derivative is maximum, based on
% % parabolic fit to the d2 curve. If the maximum of d2 is at
% % the last cycle, then this is taken to be the Cq value.
% 
% Inputs:
% y - amplification trace (1-dimensional vector)
% w - sliding window size to use for calculating first and second
% % differences
% thresh (optional) - second-derivative threshold; if maxd2 is below this
% % value, then fitting will not be performed, and the Cq value will be
% % returned as NaN
% 
% Note: x2 and d2 are shorter than x1 and d1, which are shorter than the
% input vector y. This is because at least w points before and w points
% after are required to calculate the derivative.

----------------------------------------------------------------------------------------

% [d1,d2,x1,x2,maxd2,Cq] = ...
%     calculate2ndDerivPlate(fname,nwells,windowSize,sheetNumber,thresh,outfname)
% 
% Calculate second derivatives of data in a BioRad CFX 96 or CFX 384
% amplification curve file ("...Quantification Amplification
% Results.xlsx"), and get Cq values by local quadratic fitting of the
% maximum second derivative
% 
% Outputs:
% d1 - 3-dimensional array of first derivatives for each well (A1, A2,
% % ..., B1, B2, etc.) Dimension 1 = row, dimension 2 = column, dimension 3 =
% % cycle number
% d2 - 3-dimensional array of second derivatives for each well (A1, A2,
% % ..., B1, B2, etc.) Dimension 1 = row, dimension 2 = column, dimension 3 =
% % cycle number
% x1 - cycle numbers for first derivatives
% x2 - cycle numbers for second derivatives
% maxd2 - maximum second derivative 
% Cq - 2-dimensional array of Cq values
% 
% Inputs:
% fname - xlsx input file name
% nwells - number of wells (either 96 or 384)
% windowSize - size of the window to use for calculating moving average
% sheetNumber - number of the sheet in the xlsx document in which the
% % data are stored. Set to 1 if you are using a single fluorescence 
% % channel.
% thresh (optional) - second difference threshold for scoring amplification.
% % To have no minimum threshold, you can either omit this argument or set
% % it to % [] or 0.
% outfname (optional) - file name for saving output (.mat file).

